import collections
import io

import click
import pytest


def count_twos_threes(line):
    counts = set(collections.Counter(line.strip()).values())
    frequencies = collections.Counter(counts)
    return frequencies.get(2, 0), frequencies.get(3, 0)


def checksum_boxes(stream):
    total_twos = 0
    total_threes = 0

    for line in stream:
        twos, threes = count_twos_threes(line)
        total_twos += twos
        total_threes += threes

    return total_twos * total_threes


@pytest.mark.parametrize("line,expected_twos,expected_threes", [
    ('abcdef\n', 0, 0),
    ('bababc\n', 1, 1),
    ('abbcde\n', 1, 0),
    ('abcccd\n', 0, 1),
    ('aabcdd\n', 1, 0),
    ('abcdee\n', 1, 0),
    ('ababab\n', 0, 1),
])
def test_counts(line, expected_twos, expected_threes):
    twos, threes = count_twos_threes(line)
    assert twos == expected_twos
    assert threes == expected_threes


def test_checksum():
    input = io.StringIO(
        'abcdef\n'
        'bababc\n'
        'abbcde\n'
        'abcccd\n'
        'aabcdd\n'
        'abcdee\n'
        'ababab\n')

    result = checksum_boxes(input)
    assert result == 12


@click.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part1(input):
    click.echo(checksum_boxes(input))


if __name__ == "__main__":
    part1()
