import io
import itertools

import click
import pytest


def id_difference(line_a, line_b):
    return len(line_a) - sum(a == b for a, b in zip(line_a, line_b))


def id_in_common(line_a, line_b):
    return ''.join(a for a, b in zip(line_a, line_b) if a == b)


def common_id(stream):
    lines = (line.strip() for line in stream)
    for line_a, line_b in itertools.combinations(lines, 2):
        if id_difference(line_a, line_b) == 1:
            return id_in_common(line_a, line_b)


@pytest.mark.parametrize("line_a,line_b,expected", [
    ('abcde', 'axcye', 2),
    ('fghij', 'fguij', 1),
])
def test_id_difference(line_a, line_b, expected):
    result = id_difference(line_a, line_b)
    assert result == expected


def test_id_in_common():
    result = id_in_common('fghij', 'fguij')
    assert result == 'fgij'


def test_common_id():
    input = io.StringIO(
        'abcde\n'
        'fghij\n'
        'klmno\n'
        'pqrst\n'
        'fguij\n'
        'axcye\n'
        'wvxyz\n')

    result = common_id(input)
    assert result == 'fgij'


@click.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part1(input):
    click.echo(common_id(input))


if __name__ == "__main__":
    part1()
