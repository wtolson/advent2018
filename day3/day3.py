import collections
import io

import click


Point = collections.namedtuple('Point', ['x', 'y'])
Claim = collections.namedtuple('Claim', ['id', 'offset', 'size'])


def parse_point(point, sep):
    x, y = point.split(sep)
    return Point(int(x, 10), int(y, 10))


def parse_line(line):
    id, line = line.strip().split(' @ ')
    position, size = line.split(': ')
    return Claim(id, parse_point(position, ','), parse_point(size, 'x'))


def map_fabric(stream):
    fabric = {}

    for line in stream:
        claim = parse_line(line)

        for x in range(claim.size.x):
            for y in range(claim.size.y):
                position = Point(x + claim.offset.x, y + claim.offset.y)
                fabric.setdefault(position, []).append(claim)

    return fabric


def get_overlap(fabric):
    return sum(1 for claims in fabric.values() if len(claims) > 1)


def find_nonoverlapping(fabric):
    possible = set()
    overlapping = set()

    for claims in fabric.values():
        if len(claims) == 1:
            possible.update(claims)
        else:
            overlapping.update(claims)

    return possible - overlapping


def test_parse_line():
    result = parse_line('#123 @ 3,2: 5x4')
    assert result == Claim('#123', Point(3, 2), Point(5, 4))


def test_overlap():
    input = io.StringIO(
        '#1 @ 1,3: 4x4\n'
        '#2 @ 3,1: 4x4\n'
        '#3 @ 5,5: 2x2\n')

    fabric = map_fabric(input)
    result = get_overlap(fabric)
    assert result == 4


def test_nonoverlapping():
    input = io.StringIO(
        '#1 @ 1,3: 4x4\n'
        '#2 @ 3,1: 4x4\n'
        '#3 @ 5,5: 2x2\n')

    fabric = map_fabric(input)
    result = find_nonoverlapping(fabric)
    assert result == {Claim('#3', Point(5, 5), Point(2, 2))}


@click.group()
def cli():
    pass


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part1(input):
    fabric = map_fabric(input)
    click.echo(get_overlap(fabric))


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part2(input):
    fabric = map_fabric(input)
    nonoverlapping = find_nonoverlapping(fabric)

    for claim in nonoverlapping:
        click.echo(claim.id)


if __name__ == "__main__":
    cli()
