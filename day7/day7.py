import collections
import heapq
import io

import click


TEST_STEPS = [
    ('C', 'A'),
    ('C', 'F'),
    ('A', 'B'),
    ('A', 'D'),
    ('B', 'E'),
    ('D', 'E'),
    ('F', 'E'),
]


def parse_line(line):
    return line[5], line[36]


def parse_stream(stream):
    return (parse_line(l) for l in stream)


def build_graph(steps):
    graph = collections.defaultdict(set)
    for a, b in steps:
        graph.setdefault(a, set())
        graph[b].add(a)
    return graph


def avalible_steps(graph, done):
    return (step for step, deps in graph.items() if not (deps - done))


def get_order(steps):
    graph = build_graph(steps)
    done = set()
    result = []

    while graph:
        step = min(avalible_steps(graph, done))
        del graph[step]

        done.add(step)
        result.append(step)

    return ''.join(result)


def get_time(step, offset):
    return ord(step) - 64 + offset


def get_parallel_time(steps, workers, overhead):
    graph = build_graph(steps)
    done = set()
    time = 0
    processing = []

    while graph:
        avalible = list(avalible_steps(graph, done))
        if avalible:
            heapq.heapify(avalible)

            while avalible and len(processing) < workers:
                step = heapq.heappop(avalible)
                deadline = get_time(step, overhead) + time
                heapq.heappush(processing, (deadline, step))
                del graph[step]

        time, step = heapq.heappop(processing)
        done.add(step)

    return time


def test_parse_line():
    result = parse_line('Step C must be finished before step A can begin.\n')
    assert result == ('C', 'A')


def test_parse_stream():
    stream = io.StringIO(
        'Step C must be finished before step A can begin.\n'
        'Step C must be finished before step F can begin.\n'
        'Step A must be finished before step B can begin.\n'
        'Step A must be finished before step D can begin.\n'
        'Step B must be finished before step E can begin.\n'
        'Step D must be finished before step E can begin.\n'
        'Step F must be finished before step E can begin.\n')

    result = list(parse_stream(stream))
    assert result == TEST_STEPS


def test_get_order():
    result = get_order(TEST_STEPS)
    assert result == 'CABDFE'


def test_get_parallel_time():
    result = get_parallel_time(TEST_STEPS, 2, 0)
    assert result == 15


@click.group()
def cli():
    pass


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part1(input):
    steps = list(parse_stream(input))
    result = get_order(steps)
    click.echo(result)


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part2(input):
    steps = list(parse_stream(input))
    result = get_parallel_time(steps, 5, 60)
    click.echo(result)


if __name__ == "__main__":
    cli()
