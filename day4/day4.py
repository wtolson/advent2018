import collections
import datetime
import enum
import io
import re

import click


EventTypes = enum.Enum('EventTypes', ['BEGIN', 'SLEEP', 'WAKE'])
Event = collections.namedtuple('Event', ['timestamp', 'guard', 'type'])


def parse_timestamp(value):
    return datetime.datetime.strptime(value, '%Y-%m-%d %H:%M')


def parse_line(line, guard):
    match = re.match(r'\[(\d{4}-\d{2}-\d{2} \d{2}:\d{2})\] (.+)', line)
    timestamp, event = match.groups()
    timestamp = parse_timestamp(timestamp)

    if event == 'falls asleep':
        return Event(timestamp, guard, EventTypes.SLEEP)

    if event == 'wakes up':
        return Event(timestamp, guard, EventTypes.WAKE)

    match = re.match(r'Guard #(\d+) begins shift', event)
    guard = int(match.group(1), 10)

    return Event(timestamp, guard, EventTypes.BEGIN)


def parse_event_stream(stream):
    event = Event(None, None, None)

    for line in sorted(stream):
        event = parse_line(line.strip(), event.guard)
        yield event


def most_minutes_asleep(events):
    total_sleep = collections.Counter()
    minutes_by_guard = collections.defaultdict(collections.Counter)

    for event in events:
        if event.type == EventTypes.SLEEP:
            start = event.timestamp.minute

        elif event.type == EventTypes.WAKE:
            stop = event.timestamp.minute
            total_sleep[event.guard] += stop - start
            minutes_by_guard[event.guard].update(range(start, stop))

    guard, _ = next(iter(total_sleep.most_common(1)))
    minute, _ = next(iter(minutes_by_guard[guard].most_common(1)))

    return guard, minute


def most_frequently_asleep(events):
    minutes_asleep = collections.Counter()

    for event in events:
        if event.type == EventTypes.SLEEP:
            start = event.timestamp.minute

        elif event.type == EventTypes.WAKE:
            stop = event.timestamp.minute
            minutes_asleep.update((event.guard, minute) for minute in range(start, stop))

    return minutes_asleep.most_common(1)[0][0]


TEST_EVENTS = (
    '[1518-11-01 00:00] Guard #10 begins shift\n'
    '[1518-11-01 00:05] falls asleep\n'
    '[1518-11-01 00:25] wakes up\n'
    '[1518-11-01 00:30] falls asleep\n'
    '[1518-11-01 00:55] wakes up\n'
    '[1518-11-01 23:58] Guard #99 begins shift\n'
    '[1518-11-02 00:40] falls asleep\n'
    '[1518-11-02 00:50] wakes up\n'
    '[1518-11-03 00:05] Guard #10 begins shift\n'
    '[1518-11-03 00:24] falls asleep\n'
    '[1518-11-03 00:29] wakes up\n'
    '[1518-11-04 00:02] Guard #99 begins shift\n'
    '[1518-11-04 00:36] falls asleep\n'
    '[1518-11-04 00:46] wakes up\n'
    '[1518-11-05 00:03] Guard #99 begins shift\n'
    '[1518-11-05 00:45] falls asleep\n'
    '[1518-11-05 00:55] wakes up\n')


def test_parse_line():
    result = parse_line('[1518-11-01 00:00] Guard #10 begins shift', None)
    assert result == Event(datetime.datetime(1518, 11, 1), 10, EventTypes.BEGIN)

    result = parse_line('[1518-11-01 00:05] falls asleep', 10)
    assert result == Event(datetime.datetime(1518, 11, 1, 0, 5), 10, EventTypes.SLEEP)

    result = parse_line('[1518-11-01 00:25] wakes up', 10)
    assert result == Event(datetime.datetime(1518, 11, 1, 0, 25), 10, EventTypes.WAKE)


def test_parse_event_stream():
    stream = io.StringIO(TEST_EVENTS)
    result = list(parse_event_stream(stream))

    expected = [
        Event(datetime.datetime(1518, 11, 1), 10, EventTypes.BEGIN),
        Event(datetime.datetime(1518, 11, 1, 0, 5), 10, EventTypes.SLEEP),
        Event(datetime.datetime(1518, 11, 1, 0, 25), 10, EventTypes.WAKE),
        Event(datetime.datetime(1518, 11, 1, 0, 30), 10, EventTypes.SLEEP),
        Event(datetime.datetime(1518, 11, 1, 0, 55), 10, EventTypes.WAKE),
        Event(datetime.datetime(1518, 11, 1, 23, 58), 99, EventTypes.BEGIN),
        Event(datetime.datetime(1518, 11, 2, 0, 40), 99, EventTypes.SLEEP),
        Event(datetime.datetime(1518, 11, 2, 0, 50), 99, EventTypes.WAKE),
        Event(datetime.datetime(1518, 11, 3, 0, 5), 10, EventTypes.BEGIN),
        Event(datetime.datetime(1518, 11, 3, 0, 24), 10, EventTypes.SLEEP),
        Event(datetime.datetime(1518, 11, 3, 0, 29), 10, EventTypes.WAKE),
        Event(datetime.datetime(1518, 11, 4, 0, 2), 99, EventTypes.BEGIN),
        Event(datetime.datetime(1518, 11, 4, 0, 36), 99, EventTypes.SLEEP),
        Event(datetime.datetime(1518, 11, 4, 0, 46), 99, EventTypes.WAKE),
        Event(datetime.datetime(1518, 11, 5, 0, 3), 99, EventTypes.BEGIN),
        Event(datetime.datetime(1518, 11, 5, 0, 45), 99, EventTypes.SLEEP),
        Event(datetime.datetime(1518, 11, 5, 0, 55), 99, EventTypes.WAKE),
    ]

    assert result == expected


def test_most_minutes_asleep():
    stream = io.StringIO(TEST_EVENTS)
    events = parse_event_stream(stream)
    result = most_minutes_asleep(events)
    assert result == (10, 24)


def test_most_frequently_asleep():
    stream = io.StringIO(TEST_EVENTS)
    events = parse_event_stream(stream)
    result = most_frequently_asleep(events)
    assert result == (99, 45)


@click.group()
def cli():
    pass


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part1(input):
    events = parse_event_stream(input)
    guard, minute = most_minutes_asleep(events)
    click.echo(guard * minute)


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part2(input):
    events = parse_event_stream(input)
    guard, minute = most_frequently_asleep(events)
    click.echo(guard * minute)


if __name__ == "__main__":
    cli()
