import click
import pytest


def will_react(a, b):
    return a != b and a.lower() == b.lower()


def react(polymer):
    stack = []

    for head in polymer:
        while True:
            if not stack:
                stack.append(head)
                break

            if not will_react(stack[-1], head):
                stack.append(head)
                break

            stack.pop()
            if not stack:
                break

            head = stack.pop()

    return stack


def filter_unit(polymer, unit):
    return (u for u in polymer if u.lower() != unit)


@pytest.mark.parametrize("polymer,expected", [
    ('aA', []),
    ('abBA', []),
    ('abAB', list('abAB')),
    ('aabAAB', list('aabAAB')),
    ('dabAcCaCBAcCcaDA', list('dabCBAcaDA')),
])
def test_react(polymer, expected):
    result = react(polymer)
    assert result == expected


@click.group()
def cli():
    pass


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part1(input):
    result = react(input.read().rstrip())
    click.echo(len(result))


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part2(input):
    polymer = input.read().rstrip()
    units = set(polymer.lower())
    result = min(len(react(filter_unit(polymer, u))) for u in units)
    click.echo(result)


if __name__ == "__main__":
    cli()
