import io

import click
import pytest


def do_it(value):
    return value


@pytest.mark.parametrize("value,expected", [
    ('a', 'a'),
])
def test_values(value, expected):
    result = do_it(value)
    assert result == expected


def test_stream():
    stream = io.StringIO(
        'line1\n'
        'line2\n')

    assert do_it(stream) == 42


@click.group()
def cli():
    pass


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part1(input):
    result = do_it(input.read())
    click.echo(result)


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part2(input):
    pass


if __name__ == "__main__":
    cli()
