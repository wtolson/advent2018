import collections
import re

import click
import pytest


class Marble:
    __slots__ = ('value', 'next', 'previous')

    def __init__(self, value):
        self.value = value
        self.next = None
        self.previous = None


def parse_rules(value):
    match = re.search(
        r'(\d+) players; last marble is worth (\d+) points', value)
    return tuple(int(value, 10) for value in match.groups())


def remove_marble(node):
    node.previous.next = node.next
    node.next.previous = node.previous
    node.next = node.previous = None


def insert_marble(marbles, node):
    node.next = marbles.next
    node.previous = marbles

    marbles.next.previous = node
    marbles.next = node


def play(players, last_value):
    current_marble = Marble(0)
    current_marble.next = current_marble.previous = current_marble
    scores = collections.Counter()

    for current_value in range(1, last_value + 1):
        if current_value % 23 == 0:
            for _ in range(7):
                current_marble = current_marble.previous

            marble = current_marble
            current_marble = marble.next

            score = current_value + marble.value
            scores[current_value % players] += score

            remove_marble(marble)

        else:
            marble = Marble(current_value)
            insert_marble(current_marble.next, marble)
            current_marble = marble

    return max(scores.values())


@pytest.mark.parametrize("rules,expected", [
    ('10 players; last marble is worth 1618 points\n', (10, 1618)),
    ('13 players; last marble is worth 7999 points\n', (13, 7999)),
    ('17 players; last marble is worth 1104 points\n', (17, 1104)),
    ('21 players; last marble is worth 6111 points\n', (21, 6111)),
    ('30 players; last marble is worth 5807 points\n', (30, 5807)),
])
def test_parse_rules(rules, expected):
    assert parse_rules(rules) == expected


@pytest.mark.parametrize("players,last_value,expected", [
    (10, 1618, 8317),
    (13, 7999, 146373),
    (17, 1104, 2764),
    (21, 6111, 54718),
    (30, 5807, 37305),
])
def test_play(players, last_value, expected):
    assert play(players, last_value) == expected


@click.group()
def cli():
    pass


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part1(input):
    players, last_value = parse_rules(input.read())
    result = play(players, last_value)
    click.echo(result)


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part2(input):
    players, last_value = parse_rules(input.read())
    result = play(players, 100 * last_value)
    click.echo(result)


if __name__ == "__main__":
    cli()
