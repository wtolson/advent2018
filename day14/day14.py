import math

import click
import pytest


def digits(number):
    if number == 0:
        yield 0
    else:
        for n in range(int(math.log10(number)), -1, -1):
            yield number // 10**n % 10


def to_str(numbers):
    return ''.join(str(n) for n in numbers)


def step(recipes, a, b):
    new_recipe = recipes[a] + recipes[b]
    recipes.extend(digits(new_recipe))

    a = (1 + a + recipes[a]) % len(recipes)
    b = (1 + b + recipes[b]) % len(recipes)

    return a, b


def get_ten_after(n):
    recipes = [3, 7]
    a, b = 0, 1

    while len(recipes) < (n + 10):
        a, b = step(recipes, a, b)

    return recipes[n:n+10]


def find_recipes(values):
    start = 0
    size = len(values)
    values = [int(v, 10) for v in values]

    recipes = [3, 7]
    a, b = 0, 1

    while True:
        a, b = step(recipes, a, b)

        for start in range(start, len(recipes) - size):
            if recipes[start:start+size] == values:
                return start


@pytest.mark.parametrize("value,expected", [
    (123, [1, 2, 3]),
    (0, [0]),
    (7, [7]),
    (10000, [1, 0, 0, 0, 0]),
])
def test_digits(value, expected):
    result = list(digits(value))
    assert result == expected


@pytest.mark.parametrize("recipes,a,b,expected_recipes,expected_a,expected_b", [
    ([3, 7], 0, 1, [3, 7, 1, 0], 0, 1),
    ([2, 3], 0, 1, [2, 3, 5], 0, 2),
    ([3, 7, 1, 0], 0, 1, [3, 7, 1, 0, 1, 0], 4, 3),
])
def test_step(recipes, a, b, expected_recipes, expected_a, expected_b):
    recipes = list(recipes)
    a, b = step(recipes, a, b)

    assert recipes == expected_recipes
    assert a == expected_a
    assert b == expected_b


@pytest.mark.parametrize("value,expected", [
    (9, '5158916779'),
    (5, '0124515891'),
    (18, '9251071085'),
    (2018, '5941429882'),
])
def test_get_ten_after(value, expected):
    result = get_ten_after(value)
    assert to_str(result) == expected


@pytest.mark.parametrize("value,expected", [
    ('51589', 9),
    ('01245', 5),
    ('92510', 18),
    ('59414', 2018),
])
def test_find_recipes(value, expected):
    result = find_recipes(value)
    assert result == expected


@click.group()
def cli():
    pass


@cli.command()
@click.argument('value', type=int)
def part1(value):
    result = get_ten_after(value)
    click.echo(to_str(result))


@cli.command()
@click.argument('value')
def part2(value):
    result = find_recipes(value)
    click.echo(result)


if __name__ == "__main__":
    cli()
