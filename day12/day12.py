import io

import click
import pytest


TEST_INITIAL_STATE = dict.fromkeys(
    [0, 3, 5, 8, 9, 16, 17, 18, 22, 23, 24], True)

TEST_RULES = {
    (False, False, True, False, False): True,
    (False, True, False, False, False): True,
    (False, True, False, True, False): True,
    (False, False, False, True, True): True,
    (False, True, False, True, True): True,
    (False, True, True, False, False): True,
    (False, True, True, True, True): True,
    (True, False, True, False, True): True,
    (True, False, True, True, True): True,
    (True, True, False, True, False): True,
    (True, True, False, True, True): True,
    (True, True, True, False, False): True,
    (True, True, True, False, True): True,
    (True, True, True, True, False): True,
}


class StateMap:
    def __init__(self, state=[]):
        self._state = {}
        self.start = 0
        self.stop = len(state)

        for index, value in enumerate(state):
            if value == '#':
                self._state[index] = True

    def __getitem__(self, index):
        if not isinstance(index, slice):
            return self._state.get(index, False)

        indexes = range(index.start, index.stop)
        return tuple(self._state.get(i, False) for i in indexes)

    def __setitem__(self, index, value):
        if not value:
            return

        if index < self.start:
            self.start = index

        if index >= self.stop:
            self.stop = index + 1

        self._state[index] = value

    def __len__(self):
        return len(self._state)

    def __str__(self):
        return self.to_str()

    def to_str(self, start=None, stop=None):
        if start is None:
            start = self.start

        if stop is None:
            stop = self.stop

        return ''.join('#' if self[i] else '.' for i in range(start, stop))

    def __iter__(self):
        return iter(self._state)


def parse_stream(stream):
    lines = iter(stream)
    state = StateMap(next(lines).strip().split(': ')[1])
    rules = {}

    # Skip the empty line
    next(lines)

    for line in lines:
        pattern, result = line.strip().split(' => ')
        rules[tuple(s == '#' for s in pattern)] = result == '#'

    return state, rules


def step(state, rules):
    next_state = StateMap()
    for index in range(state.start - 2, state.stop + 2):
        next_state[index] = rules.get(state[index-2:index+3], False)
    return next_state


def test_stream():
    stream = io.StringIO(
        'initial state: #..#.#..##......###...###\n'
        '\n'
        '...## => #\n'
        '..#.. => #\n'
        '.#... => #\n'
        '.#.#. => #\n'
        '.#.## => #\n'
        '.##.. => #\n'
        '.#### => #\n'
        '#.#.# => #\n'
        '#.### => #\n'
        '##.#. => #\n'
        '##.## => #\n'
        '###.. => #\n'
        '###.# => #\n'
        '####. => #\n')

    state, rules = parse_stream(stream)
    expected = dict.fromkeys(
        [0, 3, 5, 8, 9, 16, 17, 18, 22, 23, 24], True)
    assert state._state == expected
    assert rules == TEST_RULES


@pytest.mark.parametrize("state,expected", [
    ('...#..#.#..##......###...###...........',
     '...#...#....#.....#..#..#..#...........'),
    ('...#...#....#.....#..#..#..#...........',
     '...##..##...##....#..#..#..##..........'),
    ('...##..##...##....#..#..#..##..........',
     '..#.#...#..#.#....#..#..#...#..........'),
    ('..##..#..#.#....#....#..#.#...#...#....',
     '.#.#..#...#.#...##...#...#.#..##..##...'),
])
def test_step(state, expected):
    result = step(StateMap(state), TEST_RULES)
    assert result.to_str(0, len(state)) == expected


def test_part1():
    state = StateMap('#..#.#..##......###...###')

    for _ in range(20):
        state = step(state, TEST_RULES)

    expected = '.#....##....#####...#######....#.#..##.'
    assert state.to_str(-3, len(expected) - 3) == expected

    assert state.start == -2
    assert state.stop == 35

    assert sum(state) == 325


@click.command()
@click.argument('input', type=click.File('r'), default='input.txt')
@click.option('--steps', default=20)
def main(input, steps):
    state, rules = parse_stream(input)

    last_total = sum(state)
    last_delta = None

    for index in range(steps):
        state = step(state, rules)
        total = sum(state)
        delta = total - last_total

        # We found the steady state growth
        if delta == last_delta:
            total += delta * (steps - index - 1)
            break

        last_total = total
        last_delta = delta

    click.echo(total)


if __name__ == "__main__":
    main()
