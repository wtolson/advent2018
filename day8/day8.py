import collections
import io

import click


Node = collections.namedtuple('Node', ['children', 'metadata'])


def parse_node(values, start):
    result = Node([], [])
    position = start + 2

    for _ in range(values[start]):
        node, position = parse_node(values, position)
        result.children.append(node)

    for _ in range(values[start + 1]):
        result.metadata.append(values[position])
        position += 1

    return result, position


def parse_tree(stream):
    values = [int(number, 10) for number in stream.read().split()]
    node, _ = parse_node(values, 0)
    return node


def checksum_metadata(node):
    checksum = sum(node.metadata)
    checksum += sum(checksum_metadata(c) for c in node.children)
    return checksum


def node_value(root):
    if not root.children:
        return sum(root.metadata)

    result = 0

    for index in root.metadata:
        try:
            node = root.children[index - 1]
        except IndexError:
            continue

        result += node_value(node)

    return result


def test_parse_tree():
    stream = io.StringIO('2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2\n')
    expected = Node(
        children=[Node([], [10, 11, 12]), Node([Node([], [99])], [2])],
        metadata=[1, 1, 2])

    result = parse_tree(stream)
    assert result == expected


def test_checksum_metadata():
    stream = io.StringIO('2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2\n')
    tree = parse_tree(stream)
    result = checksum_metadata(tree)
    assert result == 138


def test_node_value():
    stream = io.StringIO('2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2\n')
    tree = parse_tree(stream)
    result = node_value(tree)
    assert result == 66


@click.group()
def cli():
    pass


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part1(input):
    tree = parse_tree(input)
    result = checksum_metadata(tree)
    click.echo(result)


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part2(input):
    tree = parse_tree(input)
    result = node_value(tree)
    click.echo(result)


if __name__ == "__main__":
    cli()
