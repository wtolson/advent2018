import itertools

import click
import pytest


class PowerTable:
    def __init__(self, serial):
        self.serial = serial
        self._cache = {}

    def power(self, x, y):
        rack_id = x + 10
        power = rack_id * ((rack_id * y) + self.serial)
        power = (power % 1000) // 100
        return power - 5

    def _cumultive(self, x, y):
        if x < 0 or y < 0:
            return 0

        return (
            self.cumultive(x - 1, y) +
            self.cumultive(x, y - 1) -
            self.cumultive(x - 1, y - 1) +
            self.power(x, y)
        )

    def cumultive(self, x, y):
        if (x, y) not in self._cache:
            self._cache[(x, y)] = self._cumultive(x, y)
        return self._cache[(x, y)]

    def sum(self, x, y, size):
        return (
            self.cumultive(x + size - 1, y + size - 1) -
            self.cumultive(x - 1, y + size - 1) -
            self.cumultive(x + size - 1, y - 1) +
            self.cumultive(x - 1, y - 1)
        )


def max_battery(table, size=3):
    return max(
        itertools.product(range(1, 302 - size), range(1, 302 - size)),
        key=lambda p: table.sum(p[0], p[1], size))


def max_max_battery(table, size=3):
    max_size = 1
    max_position = max_battery(table, max_size)
    max_level = table.sum(max_position[0], max_position[1], max_size)

    for size in range(2, 301):
        position = max_battery(table, size)
        level = table.sum(position[0], position[1], size)

        if level > max_level:
            max_size = size
            max_position = position
            max_level = level

    return max_size, max_position


@pytest.mark.parametrize("serial,x,y,expected", [
    (8, 3, 5, 4),
    (57, 122, 79, -5),
    (39, 217, 196, 0),
    (71, 101, 153, 4),
])
def test_power_level(serial, x, y, expected):
    table = PowerTable(serial)
    result = table.power(x, y)
    assert result == expected


@pytest.mark.parametrize("serial,x,y,expected", [
    (18, 33, 45, 29),
    (42, 21, 61, 30),
])
def test_sum_power(serial, x, y, expected):
    table = PowerTable(serial)
    result = table.sum(x, y, 3)
    assert result == expected


@pytest.mark.parametrize("serial,expected", [
    (18, (33, 45)),
    (42, (21, 61)),
])
def test_max_battery(serial, expected):
    table = PowerTable(serial)
    result = max_battery(table)
    assert result == expected


@click.group()
def cli():
    pass


@cli.command()
@click.argument('serial', type=int)
def part1(serial):
    table = PowerTable(serial)
    x, y = max_battery(table)
    click.echo(f'{x},{y}')


@cli.command()
@click.argument('serial', type=int)
def part2(serial):
    table = PowerTable(serial)
    size, (x, y) = max_max_battery(table)
    click.echo(f'{x},{y},{size}')


if __name__ == "__main__":
    cli()
