import itertools

import click
import pytest


def first_duplicate_frequency(stream):
    frequency = 0
    history = {frequency}
    numbers = itertools.cycle(int(line, 10) for line in stream)

    for number in numbers:
        frequency += number
        if frequency in history:
            return frequency
        history.add(frequency)


@click.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part2(input):
    click.echo(first_duplicate_frequency(input))


@pytest.mark.parametrize("input,expected", [
    (['+1', '-1'], 0),
    (['+3', '+3', '+4', '-2', '-4'], 10),
    (['-6', '+3', '+8', '+5', '-6'], 5),
    (['+7', '+7', '-2', '-7', '-4'], 14),
])
def test_sum_lines(input, expected):
    result = first_duplicate_frequency(input)
    assert result == expected


if __name__ == "__main__":
    part2()
