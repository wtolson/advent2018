import click
import pytest


def sum_lines(stream):
    return sum(int(number, 10) for number in stream)


@click.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part1(input):
    click.echo(sum_lines(input))


@pytest.mark.parametrize("input,expected", [
    (['+1', '+1', '+1'],  3),
    (['+1', '+1', '-2'],  0),
    (['-1', '-2', '-3'], -6),
])
def test_sum_lines(input, expected):
    result = sum_lines(input)
    assert result == expected


if __name__ == "__main__":
    part1()
