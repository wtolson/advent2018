import copy
import dataclasses
import io

import click

UP = '^'
DOWN = 'v'
LEFT = '<'
RIGHT = '>'

CART_TILES = {
    UP: '|',
    DOWN: '|',
    LEFT: '-',
    RIGHT: '-',
}


@dataclasses.dataclass
class Cart:
    x: int
    y: int
    direction: str
    intersection: int = 0

    @property
    def position(self):
        return (self.x, self.y)

    def __lt__(self, other):
        return (self.y, self.x) < (other.y, other.x)

    def __eq__(self, other):
        return (self.y, self.x) == (other.y, other.x)


def parse_cart_map(stream):
    cart_map = []
    carts = []

    for y, line in enumerate(stream):
        cart_map.append([])
        for x, tile in enumerate(line.rstrip('\n')):
            if tile in CART_TILES:
                carts.append(Cart(x, y, tile))
                tile = CART_TILES[tile]
            cart_map[y].append(tile)

    return cart_map, carts


def print_cart_map(cart_map, carts):
    cart_map = copy.deepcopy(cart_map)

    for cart in carts:
        if cart_map[cart.y][cart.x] in CART_TILES:
            cart_map[cart.y][cart.x] = 'X'
        else:
            cart_map[cart.y][cart.x] = cart.direction

    for line in cart_map:
        print(''.join(line))


def move_up(cart):
    cart.direction = UP
    cart.y -= 1


def move_down(cart):
    cart.direction = DOWN
    cart.y += 1


def move_left(cart):
    cart.direction = LEFT
    cart.x -= 1


def move_right(cart):
    cart.direction = RIGHT
    cart.x += 1


INTERSECTIONS = {
    UP: [move_left, move_up, move_right],
    DOWN: [move_right, move_down, move_left],
    LEFT: [move_down, move_left, move_up],
    RIGHT: [move_up, move_right, move_down],
}


def move_intersection(cart):
    move = INTERSECTIONS[cart.direction][cart.intersection % 3]
    cart.intersection += 1
    move(cart)


MOVES = {
    UP: {
        '|': move_up,
        '/': move_right,
        '\\': move_left,
        '+': move_intersection,
    },
    DOWN: {
        '|': move_down,
        '/': move_left,
        '\\': move_right,
        '+': move_intersection,
    },
    LEFT: {
        '-': move_left,
        '/': move_down,
        '\\': move_up,
        '+': move_intersection,
    },
    RIGHT: {
        '-': move_right,
        '/': move_up,
        '\\': move_down,
        '+': move_intersection,
    },
}


def step(cart_map, carts):
    uncrashed = {cart.position: cart for cart in carts}
    crashed = []

    for cart in carts:
        try:
            del uncrashed[cart.position]
        except KeyError:
            continue

        tile = cart_map[cart.y][cart.x]
        move = MOVES[cart.direction][tile]
        move(cart)

        if cart.position in uncrashed:
            crashed.append(uncrashed.pop(cart.position))
            crashed.append(cart)
        else:
            uncrashed[cart.position] = cart

    return crashed, sorted(uncrashed.values())


def first_crash(cart_map, carts):
    crashed = None
    carts = sorted(carts)

    while not crashed:
        crashed, carts = step(cart_map, carts)

    return crashed[0].position


def last_crash(cart_map, carts):
    carts = sorted(carts)
    while len(carts) > 1:
        _, carts = step(cart_map, carts)
    return carts[0]


TEST_MAP = [
    list('/---\\        '),
    list('|   |  /----\\'),
    list('| /-+--+-\\  |'),
    list('| | |  | |  |'),
    list('\\-+-/  \\-+--/'),
    list('  \\------/   '),
]


def test_parse_cart_map():
    stream = io.StringIO(
        '/->-\\        \n'
        '|   |  /----\\\n'
        '| /-+--+-\\  |\n'
        '| | |  | v  |\n'
        '\\-+-/  \\-+--/\n'
        '  \\------/   \n')

    tile_map, carts = parse_cart_map(stream)
    assert tile_map == TEST_MAP
    assert carts == [Cart(2, 0, RIGHT), Cart(9, 3, DOWN)]


def test_first_crash():
    crash = first_crash(TEST_MAP, [Cart(2, 0, RIGHT), Cart(9, 3, DOWN)])
    assert crash == (7, 3)


def test_last_crash():
    stream = io.StringIO(
        '/>-<\\  \n'
        '|   |  \n'
        '| /<+-\\\n'
        '| | | v\n'
        '\\>+</ |\n'
        '  |   ^\n'
        '  \\<->/\n')

    tile_map, carts = parse_cart_map(stream)
    cart = last_crash(tile_map, carts)
    assert cart.position == (6, 4)


@click.group()
def cli():
    pass


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part1(input):
    tile_map, carts = parse_cart_map(input)
    x, y = first_crash(tile_map, carts)
    click.echo(f'{x},{y}')


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part2(input):
    tile_map, carts = parse_cart_map(input)
    cart = last_crash(tile_map, carts)
    click.echo(f'{cart.x},{cart.y}')


if __name__ == "__main__":
    cli()
