import dataclasses
import re

import click


PARTICLE_RE = re.compile(
    r'position=<([- ]\d+), ([- ]\d+)> velocity=<([- ]\d+), ([- ]\d+)>')


@dataclasses.dataclass
class Vector:
    x: int
    y: int

    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vector(self.x - other.x, self.y - other.y)

    def __mul__(self, factor):
        return Vector(factor * self.x, factor * self.y)


@dataclasses.dataclass
class Particle:
    position: Vector
    velocity: Vector


def parse_particle(line):
    match = PARTICLE_RE.match(line)
    position = Vector(int(match.group(1), 10), int(match.group(2), 10))
    velocity = Vector(int(match.group(3), 10), int(match.group(4), 10))
    return Particle(position, velocity)


def parse_stream(stream):
    return [parse_particle(line) for line in stream]


def bounding_box(positions):
    lower_x = min(p.x for p in positions)
    lower_y = min(p.y for p in positions)
    upper_x = max(p.x for p in positions)
    upper_y = max(p.y for p in positions)
    return Vector(lower_x, lower_y), Vector(upper_x, upper_y)


def bounding_box_area(positions):
    lower, upper = bounding_box(positions)
    return (upper.x - lower.x) * (upper.y - lower.y)


def fast_foward(particles, time):
    return [(p.velocity * time) + p.position for p in particles]


def minimize_area(particles, learning_rate=0.8):
    time = 0

    while True:
        area = bounding_box_area(fast_foward(particles, time))
        prev_area = bounding_box_area(fast_foward(particles, time - 1))
        next_area = bounding_box_area(fast_foward(particles, time + 1))

        if area < prev_area and area < next_area:
            break

        # 0 = slope * (t - t0) + a0
        # t = (-a0 / slope) + t0
        slope = (next_area - prev_area) / 2.0
        time = int(round((-learning_rate * area / slope) + time))

    return time


def print_particles(positions):
    lower, upper = bounding_box(positions)
    size = upper - lower
    lines = [[' ' for _ in range(size.x + 1)] for _ in range(size.y + 1)]

    for position in positions:
        position = position - lower
        lines[position.y][position.x] = '#'

    for line in lines:
        print(''.join(line))


TEST_PARTICLES = [
    Particle(position=Vector(9, 1), velocity=Vector(0, 2)),
    Particle(position=Vector(7, 0), velocity=Vector(-1, 0)),
    Particle(position=Vector(3, -2), velocity=Vector(-1, 1)),
    Particle(position=Vector(6, 10), velocity=Vector(-2, -1)),
    Particle(position=Vector(2, -4), velocity=Vector(2, 2)),
    Particle(position=Vector(-6, 10), velocity=Vector(2, -2)),
    Particle(position=Vector(1, 8), velocity=Vector(1, -1)),
    Particle(position=Vector(1, 7), velocity=Vector(1, 0)),
    Particle(position=Vector(-3, 11), velocity=Vector(1, -2)),
    Particle(position=Vector(7, 6), velocity=Vector(-1, -1)),
    Particle(position=Vector(-2, 3), velocity=Vector(1, 0)),
    Particle(position=Vector(-4, 3), velocity=Vector(2, 0)),
    Particle(position=Vector(10, -3), velocity=Vector(-1, 1)),
    Particle(position=Vector(5, 11), velocity=Vector(1, -2)),
    Particle(position=Vector(4, 7), velocity=Vector(0, -1)),
    Particle(position=Vector(8, -2), velocity=Vector(0, 1)),
    Particle(position=Vector(15, 0), velocity=Vector(-2, 0)),
    Particle(position=Vector(1, 6), velocity=Vector(1, 0)),
    Particle(position=Vector(8, 9), velocity=Vector(0, -1)),
    Particle(position=Vector(3, 3), velocity=Vector(-1, 1)),
    Particle(position=Vector(0, 5), velocity=Vector(0, -1)),
    Particle(position=Vector(-2, 2), velocity=Vector(2, 0)),
    Particle(position=Vector(5, -2), velocity=Vector(1, 2)),
    Particle(position=Vector(1, 4), velocity=Vector(2, 1)),
    Particle(position=Vector(-2, 7), velocity=Vector(2, -2)),
    Particle(position=Vector(3, 6), velocity=Vector(-1, -1)),
    Particle(position=Vector(5, 0), velocity=Vector(1, 0)),
    Particle(position=Vector(-6, 0), velocity=Vector(2, 0)),
    Particle(position=Vector(5, 9), velocity=Vector(1, -2)),
    Particle(position=Vector(14, 7), velocity=Vector(-2, 0)),
    Particle(position=Vector(-3, 6), velocity=Vector(2, -1)),
]


def test_parse_particle():
    result = parse_particle('position=< 9,  1> velocity=< 0,  2>')
    assert result == Particle(Vector(9, 1), Vector(0, 2))

    result = parse_particle('position=< 7,  0> velocity=<-1,  0>')
    assert result == Particle(Vector(7, 0), Vector(-1, 0))


def test_time():
    assert minimize_area(TEST_PARTICLES) == 3


@click.group()
def cli():
    pass


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part1(input):
    particles = parse_stream(input)
    time = minimize_area(particles)
    print_particles(fast_foward(particles, time))


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part2(input):
    particles = parse_stream(input)
    time = minimize_area(particles)
    click.echo(time)


if __name__ == "__main__":
    cli()
