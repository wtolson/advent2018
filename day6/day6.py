import collections
import io

import click
import pytest


Point = collections.namedtuple('Point', ['x', 'y'])

TEST_POINTS = [
    Point(1, 1),
    Point(1, 6),
    Point(8, 3),
    Point(3, 4),
    Point(5, 5),
    Point(8, 9),
]


def parse_points(stream):
    for line in stream:
        x, y = line.strip().split(', ')
        yield Point(int(x, 10), int(y, 10))


def get_bounding_box(points):
    lower = Point(min(p.x for p in points), min(p.y for p in points))
    upper = Point(max(p.x for p in points), max(p.y for p in points))
    return lower, upper


def manhattan_distance(a, b):
    return abs(a.x - b.x) + abs(a.y - b.y)


def closest_point(points, position):
    points = iter(points)
    closest = next(points)
    min_distance = manhattan_distance(closest, position)

    for point in points:
        distance = manhattan_distance(point, position)

        if distance == min_distance:
            closest = None

        elif distance < min_distance:
            min_distance = distance
            closest = point

    return closest


def sum_distances(points, position):
    return sum(manhattan_distance(p, position) for p in points)


def largest_area(points):
    areas = collections.Counter()
    infinite = set()
    lower, upper = get_bounding_box(points)

    for x in range(lower.x, upper.x + 1):
        for y in range(lower.y, upper.y + 1):
            closest = closest_point(points, Point(x, y))
            if not closest:
                continue

            if x in (lower.x, upper.x) or y in (lower.y, upper.y):
                infinite.add(closest)

            areas[closest] += 1

    for point, area in areas.most_common():
        if point in infinite:
            continue
        return point, area

    raise ValueError('all areas are infinite')


def count_all_within(points, distance):
    lower, upper = get_bounding_box(points)
    total = 0

    for x in range(lower.x, upper.x + 1):
        for y in range(lower.y, upper.y + 1):
            if sum_distances(points, Point(x, y)) < distance:
                total += 1

    return total


def print_map(points):
    letters = iter('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    labels = {p: next(letters) for p in points}

    for point in points:
        print(f'{labels[point]}: {point}')

    infinite = set()
    points = set(points)

    lower, upper = get_bounding_box(points)

    for y in range(lower.y, upper.y + 1):
        for x in range(lower.x, upper.x + 1):
            position = Point(x, y)
            if position in points:
                print(labels[position], end='')
                continue

            closest = closest_point(points, position)
            if not closest:
                print('.', end='')
                continue

            if x in (lower.x, upper.x) or y in (lower.y, upper.y):
                infinite.add(closest)

            print(labels[closest].lower(), end='')

        print('')


def test_parse_points():
    stream = io.StringIO(
        '1, 1\n'
        '1, 6\n'
        '8, 3\n'
        '3, 4\n'
        '5, 5\n'
        '8, 9\n')

    result = list(parse_points(stream))
    assert result == TEST_POINTS


def test_bounding_box():
    result = get_bounding_box(TEST_POINTS)
    assert result == (Point(1, 1), Point(8, 9))


@pytest.mark.parametrize("position,expected", [
    (Point(5, 4), Point(5, 5)),
    (Point(5, 1), None)
])
def test_closest_point(position, expected):
    result = closest_point(TEST_POINTS, position)
    assert result == expected


def test_largest_area():
    result = largest_area(TEST_POINTS)
    assert result == (Point(5, 5), 17)


def test_count_all_within():
    result = count_all_within(TEST_POINTS, 32)
    assert result == 16


@click.group()
def cli():
    pass


@cli.command()
def print():
    print_map(TEST_POINTS)


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part1(input):
    points = list(parse_points(input))
    point, area = largest_area(points)
    click.echo(area)


@cli.command()
@click.argument('input', type=click.File('r'), default='input.txt')
def part2(input):
    points = list(parse_points(input))
    area = count_all_within(points, 10000)
    click.echo(area)


if __name__ == "__main__":
    cli()
